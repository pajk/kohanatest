<?php

use Phinx\Migration\AbstractMigration;

class AddContentToPosts extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $posts = $this->table('posts');
        $posts->addColumn('content', 'text')
              ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $posts = $this->table('posts');
        $posts->removeColumn('content')
              ->save();
    }
}